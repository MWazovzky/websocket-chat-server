const WebSocket = require('ws')

const port = process.env.PORT || 8081
const wss = new WebSocket.Server({ port })

wss.on('connection', ws => {
    ws.on('message', message => {
        console.log(`Received message: ${message}`)
        ws.send('Nice to hear from you!')
    })
    ws.send('hi everybody!')
})